from python:2-stretch

RUN apt update && apt install liblzo2-dev python-lzo mtd-utils -y
RUN pip2 install ubi_reader
RUN git clone https://github.com/jd-boyd/python-lzo.git 
RUN cd python-lzo && python setup.py install && cd .. && rm -rf python-lzo

